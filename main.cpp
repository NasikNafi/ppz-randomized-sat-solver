#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stack>
#include <chrono>
#include <map>
#include <algorithm>
#include <random>

using namespace std;

int solve();

int nVar, nClause;
vector<vector<int>> clauseList;
map<int, bool> curAssignment;

auto rng = std::default_random_engine {};
auto t1 = chrono::high_resolution_clock::now();

int read_input(char *fileName) {
    ifstream iFile(fileName);
    char temp[10];
    string str;

    // reading comments and parameters
    streampos oldpos;
    while (getline(iFile, str) && (str[0] == 'c' || str[0] == 'p')) {
        oldpos = iFile.tellg();
        if (str[0] == 'p') {
            string buf;
            stringstream ss(str);
            vector<string> tokens;
            while (ss >> buf)
                tokens.push_back(buf);
            nVar = stoi(tokens[tokens.size() - 2]);
            nClause = stoi(tokens[tokens.size() - 1]);
        }
    }

    // reading clauses
    iFile.seekg(oldpos);
    vector<int> singleClause;
    while (iFile >> temp) {
        // reading a clause with terminating 0
        if (temp[0] == '0') {
            if (singleClause.size() > 0) {
                clauseList.push_back(singleClause);
                singleClause.clear();
            }
        } else {
            int x = atoi(temp);
            singleClause.push_back(x);
        }
    }
    iFile.close();
    return 0;
}

int get_var(int x) {
    if (x < 0)
        return -x;
    else return x;
}

bool is_assigned(int var) {
    map<int, bool>::iterator itr = curAssignment.find(var);
    if (itr == curAssignment.end())
        return false;
    else
        return true;
}

bool is_unit(int clauseNo) {
    int unsatisfiedLit = 0;
    for (int j = 0; j < clauseList[clauseNo].size(); ++j) {
        if (is_assigned(get_var(clauseList[clauseNo][j]))) {
            map<int, bool>::iterator itr = curAssignment.find(get_var(clauseList[clauseNo][j]));
            if (itr != curAssignment.end()) {
                int litStatus;
                if (itr->second) {
                    litStatus = clauseList[clauseNo][j];
                } else {
                    litStatus = clauseList[clauseNo][j] * -1;
                }
                if (litStatus < 0)
                    unsatisfiedLit++;
                else
                    return false;
            }
        }
    }
    if ((clauseList[clauseNo].size() - unsatisfiedLit) == 1)
        return true;
    else
        return false;
}

int get_unassigned_lit(int clauseNo) {
    for (int j = 0; j < clauseList[clauseNo].size(); ++j) {
        if (!is_assigned(get_var(clauseList[clauseNo][j]))) {
            return clauseList[clauseNo][j];
        }
    }
    return 0;
}

int exists_unit_clause_for_var(int var) {
    for (int i = 0; i < clauseList.size(); ++i) {
        if (is_unit(i)) {
            if (var == get_var(get_unassigned_lit(i))) {
                return i;
            }
        }
    }
    return -1;
}

void print_result(bool result) {
    auto t2 = chrono::high_resolution_clock::now();
    chrono::duration<double, milli> elapsed = t2 - t1;
    cout << "c Given formula has " << nVar << " variables and " << nClause << " clauses." << endl;
    if (result) {
        cout << "s SATISFIABLE" << endl;
        cout << "v ";
        for (pair<int, bool> element : curAssignment) {
            if (element.first != 0) {
                if (element.second) {
                    cout << element.first << " ";
                } else {
                    cout << element.first * -1 << " ";
                }
            }
        }
        cout << "0" << endl;
    } else {
        cout << "s UNSATISFIABLE" << endl;
    }
    cout << "c Done (mycputime is " << elapsed.count() / 1000 << "s)." << endl;
}

int solve() {
    curAssignment.clear();

    vector<int> varOrder;
    for (int k = 1; k <= nVar; ++k) {
        varOrder.push_back(k);
    }
    std::shuffle(std::begin(varOrder), std::end(varOrder), rng);


    for (int i = 0; i < varOrder.size(); ++i) {
        int curVar = varOrder[i];
        int existsClause = exists_unit_clause_for_var(curVar);
        if (existsClause != -1) {
            int l = get_unassigned_lit(existsClause);
            if (l < 0)
                curAssignment.insert(pair<int, bool>(get_var(l), false));
            else
                curAssignment.insert(pair<int, bool>(get_var(l), true));
        } else {
            if (rand() % 2)
                curAssignment.insert(pair<int, bool>(curVar, false));
            else
                curAssignment.insert(pair<int, bool>(curVar, true));
        }
    }

    int satClause = 0;
    for (int i = 0; i < clauseList.size(); ++i) {
        for (int j = 0; j < clauseList[i].size(); ++j) {

            map<int, bool>::iterator itr = curAssignment.find(get_var(clauseList[i][j]));
            int litStatus;
            if (itr->second) {
                litStatus = clauseList[i][j];
            } else {
                litStatus = clauseList[i][j] * -1;
            }
            if (litStatus > 0) {
                satClause++;
                break;
            }
        }
    }
    if (satClause == clauseList.size())
        return 1;
    else
        return -1;

}

int main(int argc, char *argv[]) {
    // getting command line argument
    char fileName[200];
    int MAX_TRIES;
    if (argc == 3) {
        strncpy(fileName, argv[1], 200);
        MAX_TRIES = atoi(argv[2]);
    } else {
        cout << "Provide required number of arguments correctly." << endl;
        return 0;
    }

    if (read_input(fileName) == -1) {
        return 0;
    }

    for (int i = 0; i < MAX_TRIES; ++i) {
        if (solve() != -1) {
            print_result(true);
            return 0;
        }
    }
    print_result(false);

    return 0;
}